venv: venv/bin/activate

install: requirements.txt
	test -d venv || python -m virtualenv venv
	venv/bin/pip install -Ur requirements.txt
	venv/bin/python -m spacy.en.download
